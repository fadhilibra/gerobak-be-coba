package com.example.gerobakbe.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GerobakController {
    @RequestMapping("/")
    public String index() {
        return "Hello World";
    }

}
