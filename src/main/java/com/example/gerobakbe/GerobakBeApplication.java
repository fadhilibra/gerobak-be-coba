package com.example.gerobakbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GerobakBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(GerobakBeApplication.class, args);
    }

}
